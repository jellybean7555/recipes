# Cabbage Soup with Andouille Sausage

### Ingredients
* 2 cups unsalted beef stock
* 1 cup water
* 2 medium Yukon gold potatoes, peeled and cut into 1/2-inch pieces (about 8 ounces)
* 2 fresh thyme sprigs
* 4 ounces smoked andouille sausage, thinly sliced
* 1 cup chopped onion
* 3 garlic cloves, minced
* 1 (14-ounce) package coleslaw
* 1/4 cup dry white wine
* 1 (14.5-ounce) can stewed tomatoes, undrained
* 1 tablespoon red wine vinegar
* 1/4 teaspoon freshly ground black pepper

### Preparation
* Place stock, 1 cup water, potatoes, and thyme in a large saucepan; bring to a boil. 
* Reduce heat, and simmer 10 minutes or until potatoes are tender.
* Heat a large nonstick skillet over medium heat.
** Add sausage; cook 3 minutes or until lightly browned, stirring frequently. 
** Add onion; cook 3 minutes.
** Stir in garlic; cook 30 seconds. 
** Add coleslaw and wine; cook 3 minutes or until tender, stirring frequently.
* Remove thyme from potato mixture.
* Coarsely mash potatoes in pan with a potato masher.
* Add sausage mixture to potato mixture.
* Stir in tomatoes, vinegar, and pepper; simmer 3 minutes.