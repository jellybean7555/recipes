# Onion, Kale, Chickpea, and Chicken Soup
### Ingredients

* 1 tablespoon olive oil
* 1 cup prechopped onion
* 1/2 cup diagonally cut carrot
* 1/4 teaspoon crushed red pepper
* 3/8 teaspoon kosher salt
* 3 garlic cloves, crushed
* 2 thyme sprigs
* 5 cups unsalted chicken stock
* 1 (15-ounce) can unsalted chickpeas, rinsed and drained
* 2 cups chopped Lacinato kale
* 4 ounces shredded skinless, boneless rotisserie chicken thigh
* 4 ounces shredded skinless, boneless rotisserie chicken breast
* 1 teaspoon lower-sodium soy sauce

### Preparation

* Heat a large saucepan over medium-high heat.
* Add oil; swirl to coat. Add onion and next 5 ingredients (through thyme); cook 3 minutes, stirring occasionally.
* Add stock and chickpeas to pan; bring to a boil.
* Add kale and chicken to pan; reduce heat, and cook 5 minutes.
* Stir in soy sauce; discard thyme sprigs.