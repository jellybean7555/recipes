# Posole

### _Recommend Double Recipe_
### Ingredients 
* Cooking spray
* 1 (1-pound) pork tenderloin, trimmed and shredded
* 1 teaspoons salt-free Southwest chipotle seasoning blend
* 1 (15.5-ounce) can white hominy, undrained
* 1 (14.5-ounce) can Mexican-style stewed tomatoes with jalapeo peppers and spices (such as Del Monte), undrained
* 1 cup water
* 1/4 cup chopped fresh cilantro
* Avocado

### Preparation
* Heat a large saucepan over medium-high heat.
* Coat pan with olive oil.
* Sprinkle pork evenly with chipotle seasoning blend.
* Add pork to pan; cook 4 minutes or until browned.
* Stir in hominy, tomatoes, and 1 cup water. Bring to a boil; cover, reduce heat, and simmer 20 minutes or until pork is tender.
* Stir in cilantro. Top with Avocado. 