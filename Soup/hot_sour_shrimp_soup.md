# Hot and Sour Soup with Shrimp

### Ingredients
* 3 cups fat-free, less-sodium chicken broth
* 1/2 cup presliced mushrooms
* 1 tablespoon low-sodium soy sauce
* 1 (8-ounce) can sliced bamboo shoots, drained
* 2 1/2 tablespoons fresh lemon juice
* 1/4 teaspoon white pepper
* 1 1/2 pounds medium shrimp, peeled and deveined
* 1 tablespoon cornstarch
* 2 tablespoons water
* 1 large egg white, beaten
* 2 tablespoons chopped green onions
### Preparation

* Combine first 4 ingredients in a large saucepan; bring to a boil.
* Reduce heat, and simmer 5 minutes.
* Add juice, pepper, shrimp; bring to a boil.
* Cook 2 minutes or until shrimp are almost done.
* Combine cornstarch and water in a small bowl, stirring until smooth. 
* Add cornstarch mixture to pan; cook 1 minute, stirring constantly with a whisk. 
* Slowly drizzle egg white into pan, stirring constantly. 
* Remove from heat; stir in onions.