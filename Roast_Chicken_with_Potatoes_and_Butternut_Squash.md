# Roast Chicken with Potatoes and Butternut Squash

### Yield
4 servings (serving size: about 3 ounces chicken and about 3/4 cup vegetables)

### Ingredients
* 2 tablespoons minced garlic, divided
* 1 teaspoon salt, divided
* 3/4 teaspoon freshly ground black pepper, divided
* 1/2 teaspoon dried rubbed sage
* 1 (3 1/2-pound) roasting chicken
* Cooking spray
* 12 ounces red potatoes, cut into wedges
* 1 1/2 cups cubed peeled butternut squash (about 8 ounces)
* 2 tablespoons butter, melted

### Preparation
1. Preheat oven to 400°.
2. Combine 1 1/2 tablespoons garlic, 1/2 teaspoon salt, 1/2 teaspoon pepper, and sage in a small bowl. Remove and discard giblets and neck from chicken. Starting at neck cavity, loosen skin from breast and drumsticks by inserting fingers, gently pushing between skin and meat. Lift wing tips up and over back; tuck under chicken. Rub garlic mixture under loosened skin. Place chicken, breast side up, on rack of a broiler pan coated with cooking spray. Place rack in broiler pan.
3. Combine potatoes, squash, butter, 1 1/2 teaspoons garlic, 1/2 teaspoon salt, and 1/4 teaspoon pepper. Arrange vegetable mixture around chicken. Bake at 400° for 1 hour or until a thermometer inserted into meaty part of thigh registers 165°. Let stand 10 minutes. Discard skin.
