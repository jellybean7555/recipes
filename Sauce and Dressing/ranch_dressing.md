# Ranch Dressing

### Ingredients 
* 1 clove garlic
* 1 cup real mayonnaise
* 1/4 teaspoon kosher salt
* 1/2 cup sour cream
* 1/4 cup minced parsley
* 2 tablespoons dill, minced
* 1 tablespoon chives, minced
* 1 teaspoon Worcestershire sauce
* 1/2 teaspoon black pepper
* 1/2 teaspoon white vinegar
* 1/4 teaspoon paprika
* 1/8 teaspoon cayenne pepper

### Preparation
* Mince garlic then spinkle salt and mash it into a paste with a fork.
* In a bowl, combine everything.  Adjust salt/pepper as needed.

