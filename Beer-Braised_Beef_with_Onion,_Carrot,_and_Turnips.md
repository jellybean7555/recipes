# Beer-Braised Beef with Onion, Carrot, and Turnips

### Yield
4 servings (serving size: 3 ounces beef, 1 cup vegetables, and about 1/2 cup cooking liquid)

### Ingredients
* 3 tablespoons all-purpose flour
* 1 1/2 tablespoons canola oil
* 1 (1-pound) boneless chuck roast, trimmed
* 1 teaspoon salt, divided
* 1/2 teaspoon black pepper
* 1 cup fat-free, less-sodium beef broth
* 4 garlic cloves, crushed
* 1 (12-ounce) dark beer
* 1 bay leaf
* 3 carrots, peeled and cut diagonally into 1/2-inch-thick slices
* 9 ounces small turnips, peeled and cut into wedges
* 1 medium onion, peeled and cut into wedges
* 1/4 cup chopped fresh flat-leaf parsley

### Preparation
1. Preheat oven to 300°.
2. Place flour in a shallow dish. Heat oil in a Dutch oven over medium-high heat. Sprinkle beef evenly on all sides with 1/2 teaspoon salt and pepper; dredge in flour. Add beef to pan; cook 10 minutes, turning to brown on all sides. Add broth and next 3 ingredients (through bay leaf), scraping pan to remove browned bits; bring to a boil. Cover and bake at 300° for 1 1/2 hours. Add carrots; cover and cook 25 minutes. Add remaining 1/2 teaspoon salt, turnips, and onion; cover and cook an additional 1 hour and 5 minutes or until vegetables are tender and beef is fork-tender.
3. Remove beef and vegetables from pan; discard bay leaf. Cover beef mixture; keep warm. Let cooking liquid stand 10 minutes. Place a zip-top plastic bag inside a 2-cup glass measure. Pour cooking liquid into bag; let stand 10 minutes (fat will rise to the top). Seal bag; carefully snip off 1 bottom corner of bag. Drain cooking liquid into a medium bowl, stopping before fat layer reaches opening; discard fat. Serve cooking liquid with beef and vegetables. Sprinkle each serving with 1 tablespoon parsley.

### Nutritional Information

#### Amount per serving
* Calories 383 
* Fat 19.7 g 
* Satfat 6 g 
* Monofat 9.1 g 
* Polyfat 2.2 g 
* Protein 24.4 g 
* Carbohydrate 21 g 
* Fiber 3.6 g 
* Cholesterol 70 mg 
* Iron 2.9 mg 
* Sodium 815 mg 
* Calcium 68 mg 
