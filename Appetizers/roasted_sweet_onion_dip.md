# Roasted Sweet Onion Dip

### Ingredients 
* 2 large sweet onions, peeled, quartered
* 1 tablespoon olive oil
* 1 teaspoon salt
* 1 whole garlic head
* 1/3 cup reduced-fat sour cream
* 1/4 cup chopped parsley
* 1 tablespoon lemon juice

### Preparation
* Preheat oven to 425.
* Place onions in large bowl, drizzle with oil, spinkle with 1/2 teaspoon salt, toss to coat.
* Remote white papery skin from garlic head (do not peel or seperate cloves).  Wrap in foil.
* Place onion and foil wrapped on baking sheet.
* Bake for 1 hour. Cool for 10 minutes.
* Chop onion, Seperate garlic cloves, squeeze out pulp, discard skins.
* Combine onion, garlic, 1/2 teaspoon salt, sour cream, parsley, and lemon juice in large bowl.
* Cover and chill 1 hour.