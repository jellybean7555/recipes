# Chicken and Dumplings

### Yield
4 servings (serving size: about 2 cups)

### Ingredients

#### Broth
* 12 cups cold water
* 1 tablespoon whole black peppercorns
* 4 chicken leg quarters, skinned
* 3 celery stalks, sliced
* 2 medium carrots, peeled and sliced
* 2 bay leaves
* 1 large onion, peeled and cut into 8 wedges

#### Dumplings
* 4.5 ounces all-purpose flour (about 1 cup), divided
* 1 teaspoon baking powder
* 1/4 teaspoon salt
* 1/4 cup chilled butter, cut into small pieces
* 3 tablespoons buttermilk

#### Remaining ingredients
* Cooking spray
* 1 1/2 cups chopped onion
* 1 cup thinly sliced celery
* 3/4 cup (1/4-inch-thick) slices carrot
* 3/4 teaspoon salt
* 1 tablespoon all-purpose flour
* 2 tablespoons finely chopped fresh chives

### Preparation
1. To prepare broth, combine first 7 ingredients in a large stockpot; bring to a boil. Reduce heat to medium-low, and simmer 2 hours, skimming as necessary. Remove chicken from broth; cool. Remove meat from bones. Shred meat; set aside. Discard bones. Strain broth through a sieve over a bowl; discard solids. Place broth in a large saucepan; bring to a boil. Cook until reduced to 6 cups (about 8 minutes).
2. To prepare dumplings, weigh or lightly spoon 4.5 ounces (about 1 cup) flour into a dry measuring cup; level with a knife. Combine 3.4 ounces (3/4 cup) flour, baking powder, and 1/4 teaspoon salt; stir with a whisk. Cut in butter with a pastry blender or two knives until mixture resembles coarse meal. Add buttermilk; stir to combine. Turn dough out onto a lightly floured surface; knead 5 times, adding remaining 1.1 ounces (1/4 cup) flour as needed. Divide mixture into 24 equal portions.
3. Heat a large Dutch oven over medium-high heat. Coat pan with cooking spray. Add chopped onion, 1 cup celery, and 3/4 cup carrot to pan; sauté 4 minutes, stirring occasionally. Add broth and 3/4 teaspoon salt; bring to a boil. Reduce heat, and simmer 20 minutes or until vegetables are tender. Drop dumplings into pan; cover and cook 10 minutes or until dumplings are done, stirring occasionally. Remove 1/4 cup liquid from pan; stir in 1 tablespoon flour. Return chicken to pan. Add flour mixture to pan; bring to a boil. Cook 1 minute or until slightly thick, stirring occasionally. Remove from heat; stir in chives.

### Nutritional Information

#### Amount per serving
* Calories 457 
* Fat 19.7 g 
* Satfat 9.6 g 
* Monofat 5.7 g 
* Polyfat 2.4 g 
* Protein 31.8 g 
* Carbohydrate 37.1 g 
* Fiber 3.5 g 
* Cholesterol 129 mg 
* Iron 3.5 mg 
* Sodium 906 mg 
* Calcium 142 mg 
