# Pork and Sweet Potato Hash
Sweet potatoes and sliced mushrooms add an interesting twist to this familiar comfort food. Although the pork takes a while to cook, the simmering is hands off.

### Yield
4 servings (serving size: 1 1/4 cups)

### Ingredients
* 1 pound boneless pork shoulder (Boston butt), trimmed
* 1/2 teaspoon kosher salt
* 1/2 teaspoon black pepper
* Cooking spray
* 3 1/2 cups fat-free, lower-sodium chicken broth
* 6 garlic cloves, crushed
* 1 tablespoon olive oil
* 4 cups (1/2-inch) cubed peeled sweet potato (about 1 pound)
* 1 cup chopped onion
* 1/4 teaspoon ground red pepper
* 1 (8-ounce) package cremini mushrooms, quartered
* 3 tablespoons sliced green onions

### Preparation
1. Heat a 12-inch cast-iron skillet over medium-high heat. Sprinkle pork evenly with salt and black pepper. Coat pan with cooking spray. Add pork to pan, and sauté for 8 minutes, turning to brown on all sides. Add broth and garlic to pan; bring to a boil. Cover, reduce heat to low, and simmer for 45 minutes or until pork is fork-tender. Remove pork from pan, reserving cooking liquid and garlic. Cool pork slightly; shred with two forks.
2. Heat a large skillet over medium-high heat. Add olive oil to pan, and swirl to coat. Add potato and onion; sauté for 6 minutes or until lightly browned, stirring occasionally. Add red pepper and mushrooms; cook for 3 minutes. Add cooking liquid and garlic; bring to a boil. Reduce heat to medium, and cook, uncovered, for 20 minutes or until liquid nearly evaporates, stirring occasionally. Stir in pork, and cook for 1 minute or until thoroughly heated. Sprinkle with green onions.

### Nutritional Information

#### Amount per serving
* Calories 340 
* Fat 11.8 g 
* Satfat 3.4 g 
* Monofat 6.2 g 
* Polyfat 1.3 g 
* Protein 28.3 g 
* Carbohydrate 29 g 
* Fiber 5.7 g 
* Cholesterol 76 mg 
* Iron 2.9 mg 
* Sodium 732 mg 
* Calcium 78 mg 
