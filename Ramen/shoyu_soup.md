# Shoyu Soup

### Ingredients 
* 1 package ramen noodles
* 1 tablespoon sesame oil
* 2 garlic gloves, minced
* 1.5 inch ginger, minced
* 4 cup chicken stock
* 2.5 tablespoon soy saucepan
* 1/2 tablespoon sake
* 1.5 teaspoon salt
* 1 teaspoon sugar
* 1/4 cup scallions

### Preparation
* Saute ginger and garlic with sesame oil in large saucepan until fragrant
* Add soy sauce, sake, chicken stock, salt, sugar and scallions
* Simmer for a few minutes
* Put ramen noodles in boil, ladel soup over noodles to cook

### Garnishes
* Chashu pork
* Scallions
* Hard boiled egg
