# Chashu Pork

### Ingredients 
* 2 pound slab of boneless pork belly, skin-on
* 1/2 cup soy saucepan
* 2 cup sake
* 1/2 cup sugar
* 6 green onions, chopped
* 6 whole garlic gloves
* 2 inch knob of ginger, roughly sliced
* 1 whole shallot, split in half (skin on)

### Preparation
* Lay pork belly on cutting board and roll up lengthwise, with skin facing out.
* Using butcher's twine, tightly secure pork belly at 3/4-inch intervals.
* Preheat oven to 275.
* Heat up 1 cup of water, soy sauce, sake, sugar, scallions, garlic, ginger, and shallot in medium saucepan until boiling.
* Add pork belly (it won't be submerged).
* Cover with a lid left slightly ajar.
* Transfer to oven and cook, turning pork occasionally, until pork is fully tender and thin knife inserted into center meets little resistance (3 to 4 hours).
* Transfer contents to a sealed container and refridgerate until completely cool.
* Save cooking liquid for something else (sauce, broth)